//Pour utiliser notre module log -> de module simple (objet)
const logModule = require('./Modules/Simple/log')
//Ici logModule contient l'objet log que nous avons exporté
//Pour accéder à chacune de ses propriétés :
logModule.info('Projet Node.js lancé avec succès');
logModule.warning('Attention votre projet est sur le point d\'exploser');
logModule.error('Vous avez explosé');

//Pour utiliser notre module log -> de module de fonction
const logModuleFunc = require('./Modules/Func/log');
//Ici logModuleFunc contient une fonction, qu'on devra directement appeler 
logModuleFunc('Bonjour les Full Stack d\'I3, je suis Aude');


//Pour utiliser notre module person -> module de classe
const person = require('./Modules/ClassM/Person');

let aude = new person('Beurive', 'Aude', new Date(1989,9,16));
let aurelien = new person('Strimelle', 'Aurélien', new Date(1989, 10, 1));
console.log(aude.description);
console.log(aurelien.description);

//Le litteral
const bjr = require('./Modules/hello')
console.log(bjr);

////////////// EXERCICE //////////
const eventDays = require('./Modules/Exercice/eventDays');

eventDays.fromChristmas();
eventDays.fromBirthday();
eventDays.fromVacation();
eventDays.fromSolstice();
eventDays.fromFriday13();