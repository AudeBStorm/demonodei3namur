const milliInOneDay = (24*3600*1000);
const today = new Date();

function getNextDate(day, month){
    let currentYear = today.getFullYear();
    let dateMonth = month - 1;
    //On vérifie si nous sommes le même mois que la date renseignée et un ou plusieurs jours après
    //Ou que avons dépassé le mois renseigné
    if((today.getMonth() === dateMonth && today.getDate() > day) || today.getMonth() > dateMonth ){
        currentYear++;
    }
    return new Date(currentYear, dateMonth, day);
}

function getDiffDays(targetDate) {
    //On obtient la différence en millisecondes entre nos deux dates
    const diff = targetDate.getTime() - today.getTime();
    //On va diviser ce nombre total de millisecondes par le total de millisecondes, 
    //contenu dans une journée
    return Math.ceil(diff / milliInOneDay);
}

let eventDays = {
    fromChristmas : () => {
        const christmas = getNextDate(25, 12);
        const diffDays = getDiffDays(christmas);
        console.log(`Il reste ${diffDays} jours avant Noël`);
    },

    fromBirthday : function() {
        const birthday = getNextDate(16, 10);
        const diffDays = getDiffDays(birthday);
        console.log(`Il reste ${diffDays} jours avant mon anniversaire`);

    },

    fromVacation : function() {
        const vacation = getNextDate(20, 08);
        const diffDays = getDiffDays(vacation);
        console.log(`Il reste ${diffDays} jours avant mes vacances`);

    },

    fromSolstice : function() {
        const nextSummerSolstice = getNextDate(21, 6);
        const nextWinterSolstice = getNextDate(21, 12);

        const diffDaysSummer = getDiffDays(nextSummerSolstice);
        const diffDaysWinter = getDiffDays(nextWinterSolstice);

        console.log(`Il reste ${Math.min(diffDaysSummer, diffDaysWinter)} jours avant le prochain solstice`);
    },

    fromFriday13 : function() {
        let month = today.getMonth();
        //Si on a déjà dépassé le 13 du mois, on passe au mois suivant
        if(today.getDate() >= 13){
            month++;
        }

        //On crée une nouvelle date au 13 du mois suivant de l'année en cours
        //(Ici : 13 Août 2022)
        const nextFriday = new Date(today.getFullYear(), month, 13);
        //Tant que le jour de la semaine de cette date n'est pas 5 donc vendredi
        while(nextFriday.getDay() !== 5){
            //On fait +1 sur le mois de notre date
            //(Ici : 13 Septembre 2022)
            nextFriday.setMonth( nextFriday.getMonth() + 1);
            //(Dans notre cas, on va boucler jusque 13 Janvier 2023)
        }

        //On calcule ensuite la différence entre le vendredi 13 trouvé et ajd
        const diffDays = getDiffDays(nextFriday);
        console.log(`Il reste ${diffDays} jours avant le prochain Vendredi 13`);
    }
}

module.exports = eventDays;