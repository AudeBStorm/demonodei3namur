module.exports = function(lastname, firstname, birthdate){
    this.lastname = lastname;
    this.firstname = firstname;
    this.birthdate = birthdate;
    this.description = `${this.firstname} ${this.lastname} né(e) le ${this.birthdate.toDateString()}`;
}