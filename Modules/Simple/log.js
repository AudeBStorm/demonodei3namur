function getDate(){
    return new Date();
}

let log = {
    info : function(infoMsg) {
        console.log(`-> Info : ${infoMsg}`);
    },
    warning : function(warningMsg) {
        console.log(`[${getDate()}] /!\\ Warning : ${warningMsg}`)
    },
    error : function(errorMsg) {
        console.log(`:'( Error : ${errorMsg}`)
    }
}

module.exports = log;